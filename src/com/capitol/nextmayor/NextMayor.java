package com.capitol.nextmayor;

import java.util.Arrays;
import java.util.List;

public class NextMayor {
    public static String getNextMayor(Integer[] arr) {
        // Para concatenar la salida.
        StringBuilder sb = new StringBuilder();

        if(null != arr) {
            List<Integer> arrayLst = Arrays.asList(arr);

            // Tamaño del array
            final int len = arrayLst.size();
            for (int i = 0; i < len; i++) {
                // Número en la posición del array.
                Integer numActual = arrayLst.get(i);
                // Mensaje con número en el array.
                sb.append(numActual).append(" --> ");

                if(i<len-1) {
                    // Para no buscar en todo el array eliminamos la parte ya revisada
                    List<Integer> subArray = arrayLst.subList(i + 1, len);

                    // Filtramos por los mayores, buscamos el primero y si no existe devolvemos 1
                    Integer nextMayor = subArray.stream().filter(n -> n > numActual)
                        .findFirst()
                        .orElse(-1);
                    // Añadimos el número encontrado al mensaje
                    sb.append(nextMayor).append("\n");
                } else {
                    // Es el ultimo valor del array añadimos -1
                    sb.append(-1).append("\n");
                }

            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {

        Integer[] intArray = {1,2,4,32,1,41,6,12};

        String out = getNextMayor(intArray);
        System.out.println(out);

    }
}
